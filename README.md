## configure/prepare your raspberry pi

- This project assumes that python on your raspberry pi is set to python3. If you haven't already, you can configure this by running the following commands:
  `ssh pi@raspberrypi.local` // log in to your raspberry pi, adjust as needed
  `cd /usr/bin`
  `sudo rm python`
  `sudo ln -s python3.7 python`

- You'll also need docker and docker-compose on your raspberry pi:
  `curl -fsSL https://get.docker.com -o get-docker.sh`
  `sudo bash get-docker.sh`
  `sudo usermod -aG docker $(whoami)`
